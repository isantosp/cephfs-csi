# cephfs-csi

# Deploy CEPHFS-CSI Manila

Info in [deploy-cephfs](https://github.com/ceph/ceph-csi/blob/master/docs/deploy-cephfs.md)
and in here, [cloud infra docs](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/using-manila-provisioner.md#authentication-with-manila-v2-client)

## Create new OpenShift project
```yaml
oc adm new-project cephfs-csi
oc annotate project/cephfs-csi openshift.io/node-selector=""
oc project cephfs-csi
git clone https://github.com/ceph/ceph-csi.git
cd ceph-csi/deploy/cephfs/kubernetes/
```

## Create `cloud.conf`, it can be found in any other project used, for example `cinder-csi`
```yaml
cat << EOF > cloud.conf
[Global]
auth-url = <OS_AUTH_URL>
username = <OS_USERNAME>
password = <password>
domain-id = <OS_USER_DOMAIN_ID>
tenant-id = <OS_TENANT_ID>
region = <OS_REGION_NAME>
EOF
```

and create `csi-secret-cephfsplugin.yaml` doing:
```bash
oc create secret generic cloudconfig --from-file cloud.conf --dry-run -o yaml
```
then:
```bash
kubectl create -f csi-secret-cephfsplugin.yaml
```

## In order for statefulsets to work, give privileged access
```bash
oc adm policy add-scc-to-user privileged -z csi-attacher
oc adm policy add-scc-to-user privileged -z manila-provisioner
#oc adm policy add-scc-to-user privileged -z csi-provisioner
```

## Deploy RBACs for sidecar containers and node plugins:
```yaml
kubectl create -f csi-attacher-rbac.yaml
kubectl create -f csi-manila-provisioner-rbac.yaml
kubectl create -f csi-nodeplugin-rbac.yaml
#kubectl create -f csi-provisioner-rbac.yaml
```

## Deploy CSI sidecar containers:
```yaml
kubectl create -f csi-cephfsplugin-attacher.yaml
kubectl create -f csi-manila-provisioner.yaml
#kubectl create -f csi-cephfsplugin-provisioner.yaml
```

## Create ServiceAccount for manila in OpenShift
```bash
cephfs-csi-isp / manila-provisioner  ->  admin
```

## Deploy CSI CephFS driver
```yaml
oc adm policy add-scc-to-user privileged -z csi-nodeplugin
oc create -f csi-cephfsplugin.yaml
```

## Create the `StorageClass`
In order to use a mount method to use the kernel instead of fuse (which is the one selected by default) add `mounter: kernel` in this `StorageClass`

```bash
cat << EOF >  csi-sc-cephsplugin.yaml
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
  name: meyrin-cephfs
provisioner: manila-provisioner
reclaimPolicy: Retain
parameters:
  type: "Meyrin CephFS"
  zones: nova
  osSecretName: csi-cephfs-secret
  osSecretNamespace: cephfs-csi-isp
  protocol: CEPHFS
  backend: csi-cephfs
  mounter: kernel
  csi-driver: csi-cephfsplugin
EOF 
```

## Create StorageClass `secret` `csi-secret-storagevolume.yaml`

There are two ways, with a trustee or with the credentials directly, in this testing environment I created it by the credentials, in order to get a trustee, we should open a ticket to cloud.

```
cat << EOF >  csi-secret-storagevolume.yaml
apiVersion: v1
kind: Secret
metadata:
  name: csi-cephfs-secret
  namespace: cephfs-csi-isp
data:
# Required if provisionVolume is set to true
  os-authURL: <something>
  adminID: <something>
  adminKey: <something>
  os-region: <something>
  os-userName: <something>
  os-password: <something>
  os-domainID: <something>
  os-projectID: <something>
EOF
```

## Deploy StorageClass
```bash
oc create -f csi-secret-storagevolume.yaml
oc create -f csi-sc-cephsplugin.yaml
```

***
***

# Build image manila-provisioner with [patch](https://github.com/kubernetes/cloud-provider-openstack/commit/9ddec6b145090ee7fdef3a2ddda04f718bd4dae0) `makefile` in [here](https://github.com/kubernetes/cloud-provider-openstack/blob/master/Makefile#L258)

```
docker login
```

```bash
export GOOS=linux
export GOPATH=/Users/iagosantos/go

# Clone repository
#pwd= /Users/iagosantos/go/src/k8s.io/cloud-provider-openstack

git clone https://github.com/kubernetes/cloud-provider-openstack.git
cd cloud-provider-openstack/
git checkout v0.3.0
```
```
#modify Makefile to push the image directly
image-manila-provisioner: depend manila-provisioner
ifeq ($(GOOS),linux)
        cp manila-provisioner cluster/images/manila-provisioner
        docker build --no-cache -t santos171900/openstack-cloud-controller-manager:v0.3.5 cluster/images/manila-provisioner/
        docker push  santos171900/openstack-cloud-controller-manager:v0.3.5
        rm cluster/images/manila-provisioner/manila-provisioner
```
```
# Apply patch
make build 
make images
```

## Change image of the manila-provisioner
```
santos171900/openstack-cloud-controller-manager
```
